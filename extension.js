/*
 * Copyright (C) 2018 Felipe Borges <felipeborges@gnome.org>
 *
 * disable-bg-context-menu is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * gnome-shell-extension-patterns is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with gnome-shell-extension-patterns. If not, see http://www.gnu.org/licenses/.
 *
 */

const BackgroundMenu = imports.ui.backgroundMenu;
const LayoutManager = imports.ui.main.layoutManager;

function reloadMenuLayout() {
  LayoutManager._bgManagers.forEach(function(mgr) {
    if (!mgr.background)
      mgr.emit('changed');
    else // < gnome-shell-3.12?
      mgr.background.emit('changed');
  });
 }

function init() {
}

function enable() {
  BackgroundMenu.BackgroundMenu.prototype._old_init = BackgroundMenu.BackgroundMenu.prototype._init;
  BackgroundMenu.BackgroundMenu.prototype._init = function(source) {
    // Do Nothing.

  }

  reloadMenuLayout();

}

function disable() {
  BackgroundMenu.BackgroundMenu.prototype._init = BackgroundMenu.BackgroundMenu.prototype._old_init;

  reloadMenuLayout();
}
